<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../ui/aboutdialog.ui" line="14"/>
        <source>About PiSiDo</source>
        <translation type="unfinished">A PiSiDo névjegye</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="58"/>
        <source>Developer</source>
        <translation type="unfinished">Fejlesztők</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="64"/>
        <source>Main Developer</source>
        <translation type="unfinished">Főfejlesztő</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="91"/>
        <source>Hüseyin Kozan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="71"/>
        <source>http://huseyinkozan.com.tr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="98"/>
        <source>&lt;posta@huseyinkozan.com.tr&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="105"/>
        <source>Added multi package by
				     Harun Gültekin
				     &lt;hrngultekin@gmail.com&gt;</source>
        <translation type="unfinished">Csomagolta:
                                              Harun Gültekin &lt;hrngultekin@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="123"/>
        <source>Translators</source>
        <translation type="unfinished">Fordítók</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="131"/>
        <source>Turkish</source>
        <translation type="unfinished">Magyar</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="138"/>
        <source>Hüseyin Kozan &lt;posta@huseyinkozan.com.tr&gt;</source>
        <translation type="unfinished">Mogyorósi Petra &lt;squeakymouse@protonmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../ui/aboutdialog.ui" line="179"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Bezárás</translation>
    </message>
</context>
<context>
    <name>AddInstallFileLabelDialog</name>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="14"/>
        <source>Label Install File</source>
        <translation type="unfinished">Adja meg a telepítendő fájlt</translation>
    </message>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="20"/>
        <source>Path</source>
        <translation type="unfinished">Útvonal</translation>
    </message>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="27"/>
        <source>Ex: /usr/bin/*, /usr/share/__package_name__/data/image.png</source>
        <translation type="unfinished">Pl. /usr/bin/*, /usr/share/__csomagneve__/kép.png</translation>
    </message>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="37"/>
        <source>File Type</source>
        <translation type="unfinished">Fájltípus</translation>
    </message>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="98"/>
        <source>If permanent, don&apos;t delete during unisntall</source>
        <translation type="unfinished">Ne törölje eltávolításkor</translation>
    </message>
    <message>
        <location filename="../ui/addinstallfilelabeldialog.ui" line="101"/>
        <source>Permanent</source>
        <translation type="unfinished">Tartós fájl</translation>
    </message>
</context>
<context>
    <name>AddUpdateDialog</name>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="14"/>
        <source>Add Update</source>
        <translation type="unfinished">Átdolgozás hozzáadása</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="20"/>
        <source>Update</source>
        <translation type="unfinished">Átdolgozás/Frissítés</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="26"/>
        <source>Version</source>
        <translation type="unfinished">Verzió</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="33"/>
        <location filename="../ui/addupdatedialog.ui" line="36"/>
        <source>Version of the software</source>
        <translation type="unfinished">A szoftver verziója</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="43"/>
        <source>Comment</source>
        <translation type="unfinished">Megjegyzés</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="50"/>
        <location filename="../ui/addupdatedialog.ui" line="53"/>
        <source>A comment for the update</source>
        <translation type="unfinished">Ide írjon be megjegyzést az átdolgozáshoz</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="60"/>
        <source>Packager Name</source>
        <translation type="unfinished">Csomagoló neve</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="67"/>
        <location filename="../ui/addupdatedialog.ui" line="70"/>
        <source>You can set default name from application settings</source>
        <translation type="unfinished">Ha a beállításokban be van állítva, a program kitölti</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="73"/>
        <location filename="../ui/addupdatedialog.ui" line="87"/>
        <source>Set from application settings</source>
        <translation type="unfinished">Ha a beállításokban be van állítva, a program kitölti</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="80"/>
        <source>Packager E-Mail</source>
        <translation type="unfinished">Csomagoló e-mail címe</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="107"/>
        <source>Date</source>
        <translation type="unfinished">Dátum</translation>
    </message>
    <message>
        <location filename="../ui/addupdatedialog.ui" line="114"/>
        <location filename="../ui/addupdatedialog.ui" line="117"/>
        <source>The date of the update</source>
        <translation type="unfinished">Átdolgozás dátuma</translation>
    </message>
    <message>
        <location filename="../cpp/addupdatedialog.cpp" line="57"/>
        <location filename="../cpp/addupdatedialog.cpp" line="61"/>
        <location filename="../cpp/addupdatedialog.cpp" line="65"/>
        <location filename="../cpp/addupdatedialog.cpp" line="69"/>
        <source>Error</source>
        <translation type="unfinished">Hiba</translation>
    </message>
    <message>
        <location filename="../cpp/addupdatedialog.cpp" line="57"/>
        <source>Version is empty !</source>
        <translation type="unfinished">A verziószám megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/addupdatedialog.cpp" line="61"/>
        <source>Comment is empty !</source>
        <translation type="unfinished">Megjegyzés megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/addupdatedialog.cpp" line="65"/>
        <source>Packager name is empty !</source>
        <translation type="unfinished">Név megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/addupdatedialog.cpp" line="69"/>
        <source>Packager email is empty !</source>
        <translation type="unfinished">E-mail cím megadása kötelező!</translation>
    </message>
</context>
<context>
    <name>AditionalFileDialog</name>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="14"/>
        <source>Edit AdditionalFile</source>
        <translation type="unfinished">Plusz fájl hozzáadása</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="20"/>
        <source>Additional File</source>
        <translation type="unfinished">Plusz fájl</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="26"/>
        <source>File</source>
        <translation type="unfinished">Fájl</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="40"/>
        <source>Target</source>
        <translation type="unfinished">Cél</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="47"/>
        <source>Ex: /usr/share/__package_name__/, /usr/bin/xxx</source>
        <translation type="unfinished">Pl: /usr/share/__csomagneve__/, /usr/bin/xxx</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="54"/>
        <source>Permission</source>
        <translation type="unfinished">Engedélyek</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="67"/>
        <source>Read</source>
        <translation type="unfinished">Írás</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="74"/>
        <source>Write</source>
        <translation type="unfinished">Olvasás</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="81"/>
        <source>Execute</source>
        <translation type="unfinished">Futtatás</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="88"/>
        <source>User</source>
        <translation type="unfinished">Felhasználó</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="109"/>
        <location filename="../ui/aditionalfiledialog.ui" line="233"/>
        <source>Group</source>
        <translation type="unfinished">Csoport</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="137"/>
        <source>Others</source>
        <translation type="unfinished">Egyéb</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="165"/>
        <source>Special</source>
        <translation type="unfinished">Speciális</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="195"/>
        <source>UID Enabled</source>
        <translation type="unfinished">UID engedélyezése</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="202"/>
        <source>GID Enabled</source>
        <translation type="unfinished">GID engedélyezése</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="209"/>
        <source>Sticky</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="219"/>
        <source>Owner</source>
        <translation type="unfinished">Tulajdonos</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="226"/>
        <source>Owner of the target file</source>
        <translation type="unfinished">A telepített fájl tulajdonosa</translation>
    </message>
    <message>
        <location filename="../ui/aditionalfiledialog.ui" line="240"/>
        <source>Group of the target file</source>
        <translation type="unfinished">A telepített fájl csoportja</translation>
    </message>
</context>
<context>
    <name>ArchiveSelectionDialog</name>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="14"/>
        <source>Select Archive</source>
        <translation type="unfinished">Archívum kiválasztása</translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="20"/>
        <source>Archive</source>
        <translation type="unfinished">Archívum</translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="29"/>
        <location filename="../ui/archiveselectiondialog.ui" line="32"/>
        <source>Local or nonlocal archive file</source>
        <translation type="unfinished">Helyi vagy nem helyi forrásból</translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="39"/>
        <source>Browse</source>
        <translation type="unfinished">Tallózás</translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="48"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="55"/>
        <source>sha1sum value</source>
        <translation type="unfinished">SHA1 ellenőrző összeg</translation>
    </message>
    <message>
        <location filename="../ui/archiveselectiondialog.ui" line="58"/>
        <source>HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH;_</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="36"/>
        <source>Compressed Files (*.targz *.tarbz2 *.tarlzma *.tarxz *.tarZ *.tar *.zip *.gz *.gzip *.bz2 *.bzip2 *.lzma *.xz *.binary)</source>
        <translation type="unfinished">Tömörített fájlok (*.targz *.tarbz2 *.tarlzma *.tarxz *.tarZ *.tar *.zip *.gz *.gzip *.bz2 *.bzip2 *.lzma *.xz *.binary)</translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="42"/>
        <source>Select Compressed File</source>
        <translation type="unfinished">Tömörített fájl kiválasztása</translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="66"/>
        <location filename="../cpp/archiveselectiondialog.cpp" line="86"/>
        <location filename="../cpp/archiveselectiondialog.cpp" line="90"/>
        <source>Error</source>
        <translation type="unfinished">Hiba</translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="66"/>
        <source>sha1sum process timeout within 30 seconds. Please set sha1 value yourself !</source>
        <translation type="unfinished">A program nem tudta a SHA1 összeget legenerálni. Kérem, állítsa be kézzel!</translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="86"/>
        <source>Empty archive !</source>
        <translation type="unfinished">Archívum megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/archiveselectiondialog.cpp" line="90"/>
        <source>Empty sha1 !</source>
        <translation type="unfinished">Ellenőrzőösszeg megadása kötelező!</translation>
    </message>
</context>
<context>
    <name>ArchiveWidget</name>
    <message>
        <location filename="../ui/archivewidget.ui" line="14"/>
        <source>ArchiveWidget</source>
        <translation type="unfinished">ArchiveWidget</translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="41"/>
        <source>Arch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="48"/>
        <location filename="../ui/archivewidget.ui" line="51"/>
        <source>Archive path</source>
        <translation type="unfinished">Archívum elérési útvonala</translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="65"/>
        <location filename="../ui/archivewidget.ui" line="68"/>
        <source>Archive SHA1 value</source>
        <translation type="unfinished">Archívum SHA1 összege</translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="71"/>
        <source>HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH;_</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="78"/>
        <location filename="../ui/archivewidget.ui" line="81"/>
        <source>Archive type</source>
        <translation type="unfinished">Fájltípus</translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="88"/>
        <location filename="../ui/archivewidget.ui" line="91"/>
        <source>Remove archive from list</source>
        <translation type="unfinished">Archívum eltávolítása</translation>
    </message>
    <message>
        <location filename="../ui/archivewidget.ui" line="58"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ConfigurationDialog</name>
    <message>
        <location filename="../ui/configurationdialog.ui" line="14"/>
        <source>Configuration</source>
        <translation type="unfinished">Beállítások</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="24"/>
        <source>General</source>
        <translation type="unfinished">Általános</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="43"/>
        <source>Packager</source>
        <translation type="unfinished">Csomagoló</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="49"/>
        <source>Name</source>
        <translation type="unfinished">Név</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="56"/>
        <location filename="../ui/configurationdialog.ui" line="70"/>
        <source>This will use while adding an update to the history section</source>
        <translation type="unfinished">Ez a frissítések nyomonkövetéséhez szükséges</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="63"/>
        <source>E-Mail</source>
        <translation type="unfinished">E-mail</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="81"/>
        <source>Directories</source>
        <translation type="unfinished">Könyvtárak</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="87"/>
        <source>PiSi packaging dir</source>
        <translation type="unfinished">Csomagolókörnyezet helye</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="94"/>
        <location filename="../ui/configurationdialog.ui" line="128"/>
        <location filename="../ui/configurationdialog.ui" line="142"/>
        <source>Used only opening from menu as shortcut</source>
        <translation type="unfinished">Ez a menüsávból érhető el</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="115"/>
        <source>Web Pages</source>
        <translation type="unfinished">Online erőforrások</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="121"/>
        <source>Actions API Page</source>
        <translation type="unfinished">PISI API dokumentáció</translation>
    </message>
    <message>
        <location filename="../ui/configurationdialog.ui" line="135"/>
        <source>PISI Spec File</source>
        <translation type="unfinished">PISI fájlspecifikáció</translation>
    </message>
</context>
<context>
    <name>DirectoryModel</name>
    <message>
        <location filename="../cpp/directorymodel.cpp" line="256"/>
        <source>Name</source>
        <translation type="unfinished">Név</translation>
    </message>
    <message>
        <location filename="../cpp/directorymodel.cpp" line="259"/>
        <source>Permission</source>
        <translation type="unfinished">Engedélyek</translation>
    </message>
    <message>
        <location filename="../cpp/directorymodel.cpp" line="262"/>
        <source>Size</source>
        <translation type="unfinished">Méret</translation>
    </message>
    <message>
        <location filename="../cpp/directorymodel.cpp" line="265"/>
        <source>Symlink</source>
        <translation type="unfinished">Hivatkozás</translation>
    </message>
</context>
<context>
    <name>LanguageDialog</name>
    <message>
        <location filename="../ui/languagedialog.ui" line="14"/>
        <source>Select Language</source>
        <translation type="unfinished">Nyelv kiválasztása</translation>
    </message>
    <message>
        <location filename="../ui/languagedialog.ui" line="20"/>
        <source>Language</source>
        <translation type="unfinished">Alkalmazás nyelve</translation>
    </message>
    <message>
        <location filename="../ui/languagedialog.ui" line="30"/>
        <source>Languge change will apply after application restart.</source>
        <translation type="unfinished">A beállítások a legközelebbi indításkor lépnek életbe.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>PiSiDo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="383"/>
        <location filename="../ui/mainwindow.ui" line="386"/>
        <source>Give directory name for import !</source>
        <translation type="unfinished">Adja meg a csomagbeállító fájlok helyét</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="396"/>
        <location filename="../ui/mainwindow.ui" line="399"/>
        <source>Import package</source>
        <translation type="unfinished">Csomag importálása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="416"/>
        <location filename="../ui/mainwindow.ui" line="419"/>
        <source>Open Package Directory</source>
        <translation type="unfinished">Csomagoló munkamappa megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="76"/>
        <source>Summary</source>
        <translation type="unfinished">Rövid leírás</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="266"/>
        <location filename="../ui/mainwindow.ui" line="269"/>
        <source>A brief description for software that packaging</source>
        <translation type="unfinished">A csomag rövid leírása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="121"/>
        <source>Package Name</source>
        <translation type="unfinished">Csomag neve</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="128"/>
        <source>Homepage</source>
        <translation type="unfinished">Weboldal</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="256"/>
        <location filename="../ui/mainwindow.ui" line="259"/>
        <source>Homepage of the software that packaging</source>
        <translation type="unfinished">A program weboldala</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="135"/>
        <source>Build Dep.</source>
        <translation type="unfinished">Fordítási függőségek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="194"/>
        <location filename="../ui/mainwindow.ui" line="210"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Comma seperated package names.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ex:Qt From 4.6.0 And Gtk To 2.4 : &lt;span style=&quot; font-weight:600;&quot;&gt;qt[&amp;gt;4.6.0], gtk[&amp;lt;2.4]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ex:Qt exactly 4.6.0 And Gtk : &lt;span style=&quot; font-weight:600;&quot;&gt;qt[=4.6.0], gtk&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A csomagok neveit vesszővel válassza el!&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pl: Qt 4.6.0 és újabb, Gtk 2.4 és régebbi : &lt;span style=&quot; font-weight:600;&quot;&gt;qt[&amp;gt;4.6.0], gtk[&amp;lt;2.4]&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pl: csak Qt 4.6.0 és Gtk : &lt;span style=&quot; font-weight:600;&quot;&gt;qt[=4.6.0], gtk&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="203"/>
        <location filename="../ui/mainwindow.ui" line="219"/>
        <source>Comma seperated package names. Ex: qt[&gt;4.4], gtk, glibc[&gt;2]</source>
        <translation type="unfinished">A csomagok neveit vesszővel válassza el (pl. qt[ &gt;4.4], gtk3, glibc[&gt;2])</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="155"/>
        <source>Runtime Dep.</source>
        <translation type="unfinished">Futtatási függőségek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="96"/>
        <source>Licence</source>
        <translation type="unfinished">Licenc</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="114"/>
        <source>Type</source>
        <translation type="unfinished">Típus</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="49"/>
        <source>Component</source>
        <translation type="unfinished">Kategória</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="246"/>
        <location filename="../ui/mainwindow.ui" line="249"/>
        <source>Select component</source>
        <translation type="unfinished">Válassza ki a megfelelő kategóriát</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="66"/>
        <source>Archive</source>
        <translation type="unfinished">Archívum</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="336"/>
        <location filename="../ui/mainwindow.ui" line="339"/>
        <source>Use compressed type to compute sha1 values of local archive files</source>
        <translation type="unfinished">Helyi tömörített archívum esetén a SHA1 érték automatikusan kiszámításra kerül</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="343"/>
        <source>Compressed</source>
        <translation type="unfinished">Helyi</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="348"/>
        <source>Url</source>
        <translation type="unfinished">Online (URL)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="356"/>
        <location filename="../ui/mainwindow.ui" line="359"/>
        <source>Add archive</source>
        <translation type="unfinished">Archívum hozzáadása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="276"/>
        <location filename="../ui/mainwindow.ui" line="279"/>
        <source>Detailed description for software that packaging</source>
        <translation type="unfinished">A csomagolt szoftver bővebb leírása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="142"/>
        <source>Description</source>
        <translation type="unfinished">Leírás</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="226"/>
        <location filename="../ui/mainwindow.ui" line="229"/>
        <source>Comma seperated licenses</source>
        <translation type="unfinished">Az elemeket vesszővel válassza el!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="236"/>
        <location filename="../ui/mainwindow.ui" line="239"/>
        <source>Comma seperated types</source>
        <translation type="unfinished">Az elemeket vesszővel válassza el!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="56"/>
        <source>Translation</source>
        <translation type="unfinished">Fordítások</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="89"/>
        <source>Packages</source>
        <translation type="unfinished">Csomagok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="449"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fájl</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="460"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;Beállítások</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="468"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Súgó</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="480"/>
        <source>&amp;Tools</source>
        <translation type="unfinished">&amp;Eszközök</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="488"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Nézet</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="504"/>
        <location filename="../ui/mainwindow.ui" line="507"/>
        <source>History Window</source>
        <translation type="unfinished">Történet</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="510"/>
        <source>History</source>
        <translation type="unfinished">Történet</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="520"/>
        <location filename="../ui/mainwindow.ui" line="523"/>
        <source>You have to add at least one update</source>
        <translation type="unfinished">Legalább egy frissítésnek jelen kell lennie</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="554"/>
        <source>Rel. No</source>
        <translation type="unfinished">Kiadás</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="559"/>
        <source>Date</source>
        <translation type="unfinished">Dátum</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="564"/>
        <source>Version</source>
        <translation type="unfinished">Verzió</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="569"/>
        <source>Comment</source>
        <translation type="unfinished">Megjegyzés</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="574"/>
        <location filename="../ui/mainwindow.ui" line="1235"/>
        <source>Name</source>
        <translation type="unfinished">Név</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="579"/>
        <source>Email</source>
        <translation type="unfinished">E-Mail</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="587"/>
        <location filename="../ui/mainwindow.ui" line="590"/>
        <source>Add an update</source>
        <translation type="unfinished">Új frissítés</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="604"/>
        <location filename="../ui/mainwindow.ui" line="607"/>
        <source>Delete last update</source>
        <translation type="unfinished">Legfrissebb törlése</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="627"/>
        <location filename="../ui/mainwindow.ui" line="630"/>
        <source>Actions File Window</source>
        <translation type="unfinished">Csomagolószkript</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="645"/>
        <source>Select Template </source>
        <translation type="unfinished">Sablon:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="652"/>
        <location filename="../ui/mainwindow.ui" line="655"/>
        <source>Select actions.py template</source>
        <translation type="unfinished">Kiválaszthat egy actions.py sablont</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="694"/>
        <source>Imported</source>
        <translation type="unfinished">Importált</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="715"/>
        <location filename="../ui/mainwindow.ui" line="718"/>
        <source>Zoom In</source>
        <translation type="unfinished">Nagyítás</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="732"/>
        <location filename="../ui/mainwindow.ui" line="735"/>
        <source>Zoom Out</source>
        <translation type="unfinished">Kicsinyítés</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="757"/>
        <location filename="../ui/mainwindow.ui" line="760"/>
        <source>Build Window</source>
        <translation type="unfinished">Csomagolóműveletek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="763"/>
        <source>Build</source>
        <translation type="unfinished">Csomagolás</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="779"/>
        <source>Build Commands</source>
        <translation type="unfinished">Csomagolóműveletek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="787"/>
        <source>Only</source>
        <translation type="unfinished">Csak</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="795"/>
        <source>Build Files</source>
        <translation type="unfinished">Beállítófájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1682"/>
        <location filename="../ui/mainwindow.ui" line="1685"/>
        <source>Open Log</source>
        <translation type="unfinished">Napló</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1688"/>
        <source>Open Log File</source>
        <translation type="unfinished">Eseménynapló megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="186"/>
        <location filename="../ui/mainwindow.ui" line="800"/>
        <source>Package</source>
        <translation type="unfinished">Csomag</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="808"/>
        <location filename="../ui/mainwindow.ui" line="811"/>
        <source>Build only selected</source>
        <translation type="unfinished">Csak a beállítófájlokat menti</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="829"/>
        <source>Up To</source>
        <translation type="unfinished">Eddig:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="870"/>
        <location filename="../ui/mainwindow.ui" line="873"/>
        <source>Build up to selected (see pisi help build)</source>
        <translation type="unfinished">A folyamat a kiválasztott lépésig meg végbe (lásd pisi help build)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="891"/>
        <source>Build All</source>
        <translation type="unfinished">Mindet</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="898"/>
        <location filename="../ui/mainwindow.ui" line="901"/>
        <source>Create build files in to the package directory and builds package</source>
        <translation type="unfinished">Beállítófájlok mentése és csomag elkészítése</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="939"/>
        <location filename="../ui/mainwindow.ui" line="942"/>
        <source>Install Files Window</source>
        <translation type="unfinished">Telepített fájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1093"/>
        <location filename="../ui/mainwindow.ui" line="1096"/>
        <location filename="../ui/mainwindow.ui" line="1179"/>
        <location filename="../ui/mainwindow.ui" line="1182"/>
        <location filename="../ui/mainwindow.ui" line="1341"/>
        <location filename="../ui/mainwindow.ui" line="1344"/>
        <source>Please refresh after a successful build !</source>
        <translation type="unfinished">Töltse újra, ha a próbatelepítés sikerült</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="955"/>
        <location filename="../ui/mainwindow.ui" line="958"/>
        <source>A successful build need for listing. Which will fill /var/pisi/__package_name__/install</source>
        <translation type="unfinished">Csak sikeres próbatelepítés esetén fognak itt fájlok megjelenni. A fájlok a /var/pisi/__csomagneve__/install mappában lesznek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="965"/>
        <location filename="../ui/mainwindow.ui" line="968"/>
        <source>If none, a default will be added</source>
        <translation type="unfinished">Ha nincs itt semmi, akkor a program elhelyez egy alapértéket</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="987"/>
        <location filename="../ui/mainwindow.ui" line="1392"/>
        <source>File</source>
        <translation type="unfinished">Fájl</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="992"/>
        <source>File Type</source>
        <translation type="unfinished">Típus</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="997"/>
        <source>Permanent</source>
        <translation type="unfinished">Tartós</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1063"/>
        <location filename="../ui/mainwindow.ui" line="1066"/>
        <source>Opens install directory (/var/pisi/__package_name__/install)</source>
        <translation type="unfinished">Próbatelepítési mappa (/var/pisi/__csomagneve__/install) megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1008"/>
        <location filename="../ui/mainwindow.ui" line="1011"/>
        <source>Delete selected label</source>
        <translation type="unfinished">Kiválasztott címke törlése</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1028"/>
        <location filename="../ui/mainwindow.ui" line="1031"/>
        <source>Add label</source>
        <translation type="unfinished">Új címke</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1083"/>
        <location filename="../ui/mainwindow.ui" line="1086"/>
        <source>Directory of the installed files</source>
        <translation type="unfinished">Próbatelepítési könyvtár</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1134"/>
        <location filename="../ui/mainwindow.ui" line="1137"/>
        <source>Patches Window</source>
        <translation type="unfinished">Patchek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1140"/>
        <source>Patches</source>
        <translation type="unfinished">Patchek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1198"/>
        <source>Add your patches to package files directory (workspace/__package_name__/files/).
And than you can define priority in here.</source>
        <translation type="unfinished">A patchek a /környezet/__csomagneve__/files mappában lesznek elhelyezve. A patchek prioritását itt adhatja meg</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1202"/>
        <source>Add your patches to package files directory (workspace/__package_name__/files/). And than you can define priority in here.</source>
        <translation type="unfinished">A patchek a /környezet/__csomagneve__/files mappában lesznek elhelyezve. A patchek prioritását itt adhatja meg</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1230"/>
        <source>Level</source>
        <translation type="unfinished">Szint</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1243"/>
        <location filename="../ui/mainwindow.ui" line="1246"/>
        <source>Level decrease (will operate earlier)</source>
        <translation type="unfinished">Szint emelése (előrevétel)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1260"/>
        <location filename="../ui/mainwindow.ui" line="1263"/>
        <source>Level increase (will operate later)</source>
        <translation type="unfinished">Szint csökkentése (hátravétel)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1155"/>
        <location filename="../ui/mainwindow.ui" line="1158"/>
        <source>Opens patch directory (workspace/__package_name__/files/)</source>
        <translation type="unfinished">A patchek helyének (/környezet/__csomagneve__/files) megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1296"/>
        <location filename="../ui/mainwindow.ui" line="1299"/>
        <source>Aditional Files Window</source>
        <translation type="unfinished">Plusz fájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1360"/>
        <source>Add your aditional files to package files directory (workspace/__package_name__/files/).
And than you can edit their properties in here</source>
        <translation type="unfinished">Itt adhat meg plusz fájlokat és a beállításaikat. A fájlok a /környezet/__csomagneve__/files mappában lesznek tárolva</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1364"/>
        <source>Add your aditional files to package files directory (workspace/__package_name__/files/).\nAnd than you can edit their properties in here</source>
        <translation type="unfinished">Itt adhat meg plusz fájlokat és a beállításaikat. A fájlok a /környezet/__csomagneve__/files mappában lesznek tárolva</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1397"/>
        <source>Target</source>
        <translation type="unfinished">Cél</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1402"/>
        <source>Permission</source>
        <translation type="unfinished">Engedélyek</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1407"/>
        <source>Owner</source>
        <translation type="unfinished">Tulajdonos</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1412"/>
        <source>Group</source>
        <translation type="unfinished">Csoport</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1420"/>
        <location filename="../ui/mainwindow.ui" line="1423"/>
        <source>Edits selected file</source>
        <translation type="unfinished">Kiválasztott fájl szerkesztése</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1317"/>
        <location filename="../ui/mainwindow.ui" line="1320"/>
        <source>Opens aditional files directory (workspace/__package_name__/files/)</source>
        <translation type="unfinished">A plusz fájlok helyének (/környezet/__csomagneve__/files) megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1452"/>
        <source>Operations Toolbar</source>
        <translation type="unfinished">Műveleti eszköztár</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1473"/>
        <source>View Toolbar</source>
        <translation type="unfinished">Nézet eszköztár</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1487"/>
        <source>Help Toolbar</source>
        <translation type="unfinished">Súgó eszköztár</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1511"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">&amp;Kilépés</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1514"/>
        <source>Exit</source>
        <translation type="unfinished">Kilépés</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1517"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1526"/>
        <source>&amp;Configure...</source>
        <translation type="unfinished">&amp;Beállító ablak</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1538"/>
        <source>A&amp;bout...</source>
        <translation type="unfinished">&amp;Névjegy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1577"/>
        <source>&amp;Actions API</source>
        <translation type="unfinished">&amp;Szkript API</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1592"/>
        <source>&amp;PISI Spec</source>
        <translation type="unfinished">&amp;PISI fájlspecifikáció</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1607"/>
        <source>&amp;Language...</source>
        <translation type="unfinished">&amp;Nyelv</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1622"/>
        <source>Change &amp;WS</source>
        <translation type="unfinished">Munkakörnyezet módosítása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1637"/>
        <source>&amp;Open WS</source>
        <translation type="unfinished">&amp;Munkakörnyezet megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1652"/>
        <source>&amp;Reset</source>
        <translation type="unfinished">&amp;Visszaállít</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1529"/>
        <source>Configure Application</source>
        <translation type="unfinished">Alkalmazás konfigurálása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="633"/>
        <source>Actions</source>
        <translation type="unfinished">Műveleti szkript</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="945"/>
        <source>Installs</source>
        <translation type="unfinished">Telepített fájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1118"/>
        <source>&lt;Files&gt;</source>
        <translation type="unfinished">Fájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1302"/>
        <source>Aditionals</source>
        <translation type="unfinished">Plusz fájlok</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1541"/>
        <source>About</source>
        <translation type="unfinished">Névjegy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1550"/>
        <source>About &amp;Qt...</source>
        <translation type="unfinished">A &amp;Qt névjegye</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1553"/>
        <source>About Qt</source>
        <translation type="unfinished">A Qt névjegye</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1562"/>
        <source>&amp;Help...</source>
        <translation type="unfinished">&amp;Súgó</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1565"/>
        <source>Help</source>
        <translation type="unfinished">Súgó</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1568"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1580"/>
        <location filename="../ui/mainwindow.ui" line="1583"/>
        <source>Opens Actions API web page</source>
        <translation type="unfinished">A szkript API dokumentáció megtekintése (online)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1595"/>
        <location filename="../ui/mainwindow.ui" line="1598"/>
        <source>Opens PISI Spec web page</source>
        <translation type="unfinished">A PISI fájlspecifikáció megtekintése (online)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1610"/>
        <location filename="../ui/mainwindow.ui" line="1613"/>
        <source>Change Application Language</source>
        <translation type="unfinished">Alkalmazás nyelvének kiválasztása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1625"/>
        <location filename="../ui/mainwindow.ui" line="1628"/>
        <source>Change Workspace</source>
        <translation type="unfinished">Munkakörnyezet beállítása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1640"/>
        <location filename="../ui/mainwindow.ui" line="1643"/>
        <source>Open Workspace</source>
        <translation type="unfinished">Munkakörnyezet megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1655"/>
        <location filename="../ui/mainwindow.ui" line="1658"/>
        <source>Reset Fields</source>
        <translation type="unfinished">Mezők törlése</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1667"/>
        <source>Open PP&amp;D</source>
        <translation type="unfinished">&amp;Csomagolókönytár megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1670"/>
        <source>Open Pisi Packaging Directory</source>
        <translation type="unfinished">&amp;Csomagolókönytár megnyitása</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1673"/>
        <source>Open PISI Packaging Directory</source>
        <translation type="unfinished">&amp;Csomagolókönytár megnyitása</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="83"/>
        <location filename="../cpp/mainwindow.cpp" line="84"/>
        <location filename="../cpp/mainwindow.cpp" line="85"/>
        <source>Show or hide %1</source>
        <translation type="unfinished">%1 megjelenítése/elrejtése</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="184"/>
        <source>No Workspace</source>
        <translation type="unfinished">Érvénytelen környezet</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="185"/>
        <source>Workspace does not exists !</source>
        <translation type="unfinished">A munkakörnyezet nem létezik</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="245"/>
        <location filename="../cpp/mainwindow.cpp" line="276"/>
        <location filename="../cpp/mainwindow.cpp" line="365"/>
        <location filename="../cpp/mainwindow.cpp" line="1110"/>
        <location filename="../cpp/mainwindow.cpp" line="1138"/>
        <location filename="../cpp/mainwindow.cpp" line="1142"/>
        <location filename="../cpp/mainwindow.cpp" line="1150"/>
        <location filename="../cpp/mainwindow.cpp" line="1154"/>
        <location filename="../cpp/mainwindow.cpp" line="1478"/>
        <location filename="../cpp/mainwindow.cpp" line="1482"/>
        <location filename="../cpp/mainwindow.cpp" line="1489"/>
        <location filename="../cpp/mainwindow.cpp" line="1493"/>
        <location filename="../cpp/mainwindow.cpp" line="1497"/>
        <location filename="../cpp/mainwindow.cpp" line="1599"/>
        <source>Error</source>
        <translation type="unfinished">Hiba</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="245"/>
        <source>Can not open &quot;%1&quot; resource !</source>
        <translation type="unfinished">&quot;%1&quot; erőforrás nem nyitható meg!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="276"/>
        <source>Can not open %1 file to write.</source>
        <translation type="unfinished">&quot;%1&quot; fájl nem nyitható meg írásra</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="365"/>
        <source>There are no translation files in : %1</source>
        <translation type="unfinished">Nincs fordítófájl a %1 mappában</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="452"/>
        <source>http://wiki.pisilinux.org/tr/index.php?title=ActionsAPI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="454"/>
        <source>http://svn.pardus.org.tr/uludag/trunk/pisi/pisi-spec.rng</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="534"/>
        <location filename="../cpp/mainwindow.cpp" line="1394"/>
        <source>Error at conversion release string to integer !</source>
        <translation type="unfinished">A kiadásszám nem konvertálható számadattá, mert érvénytelen karaktereket tartalmaz</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="966"/>
        <source>True</source>
        <translation type="unfinished">Igaz</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="966"/>
        <source>False</source>
        <translation type="unfinished">Hamis</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1110"/>
        <source>Can not open file for reading !</source>
        <translation type="unfinished">A fájl nem olvasható!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1123"/>
        <source>Parse Error</source>
        <translation type="unfinished">Ellenőrzési hiba</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1124"/>
        <location filename="../cpp/mainwindow.cpp" line="1259"/>
        <source>XML Parse Error : 
%1
Line:%2, Column:%3</source>
        <translation type="unfinished">XML ellenőrzési hiba itt: %1., %2 sor %3. oszlop</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1138"/>
        <source>An error occured while parsing xml file : %1</source>
        <translation type="unfinished">Hiba történt a &quot;%1&quot; XML fájl olvasása közben</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1142"/>
        <location filename="../cpp/mainwindow.cpp" line="1154"/>
        <source>Unknownt exception !</source>
        <translation type="unfinished">Váratlan hiba</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1150"/>
        <source>An error occured while filling fields: %1</source>
        <translation type="unfinished">Hiba történt a(z) %1 mező beállítása közben</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1238"/>
        <location filename="../cpp/mainwindow.cpp" line="1246"/>
        <location filename="../cpp/mainwindow.cpp" line="1253"/>
        <location filename="../cpp/mainwindow.cpp" line="1258"/>
        <source>Translation File Error</source>
        <translation type="unfinished">Fordítófájl-hiba</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1239"/>
        <source>No Name tag in Source tag in PISI tag in translation.xml or name is not same with package name !</source>
        <translation type="unfinished">A translation.xml fájlban nincsen &lt;Name&gt; tag vagy más csomaghoz tartozik</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1247"/>
        <source>No Source tag in PISI tag in translation.xml !</source>
        <translation type="unfinished">A translation.xml fájlban nincsen &lt;Source&gt;</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1254"/>
        <source>No PISI tag in translation.xml !</source>
        <translation type="unfinished">A translation.xml fájlban nincsen &lt;PISI&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1279"/>
        <source>Empty pisi file, import pspec.xml before use !</source>
        <translation type="unfinished">Nincsen beállítófájl. Kérem importáljon be egyet (pspec.xml)!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1693"/>
        <source>Elapsed Time</source>
        <translation type="unfinished">Eltelt idő</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1693"/>
        <source>   %1:%2:%3   </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1731"/>
        <location filename="../cpp/mainwindow.cpp" line="1763"/>
        <location filename="../cpp/mainwindow.cpp" line="1769"/>
        <location filename="../cpp/mainwindow.cpp" line="1773"/>
        <location filename="../cpp/mainwindow.cpp" line="1777"/>
        <source>Warning</source>
        <translation type="unfinished">Figyelmeztetés</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1731"/>
        <source>Please enter package data.</source>
        <translation type="unfinished">Kérem adja meg a csomagbeállító fájlt</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1763"/>
        <location filename="../cpp/mainwindow.cpp" line="1769"/>
        <source>File is not opened!</source>
        <translation type="unfinished">A fájl nem nyitható meg</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1773"/>
        <source>Any log file is not found!</source>
        <translation type="unfinished">A napló nem található</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1777"/>
        <source>Please try again after build process.</source>
        <translation type="unfinished">Kérem próbálja újra lefordítás után</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1271"/>
        <source>Package build information successfully imported.</source>
        <translation type="unfinished">Csomagbeállító információk</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1386"/>
        <source>Please define an update in history !</source>
        <translation type="unfinished">Kérem adjon meg egy átdolgozást</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1478"/>
        <source>An error occured while filling pisi from fields :
 %1</source>
        <translation type="unfinished">Hiba történt a %1 mező átvitele közben</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1482"/>
        <source>Unknownt exception while filling pisi from fields !</source>
        <translation type="unfinished">Váratlan hiba történt az adatok átvitele közben</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1489"/>
        <source>Empty package name !</source>
        <translation type="unfinished">Nincs csomagnév!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1493"/>
        <source>Can not create package directory in workspace !</source>
        <translation type="unfinished">A csomag könyvtára nem hozható létre!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1497"/>
        <source>Can not create files directory in package directory !</source>
        <translation type="unfinished">A files könyvtár nem hozható létre!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1547"/>
        <source>Actions API File</source>
        <translation type="unfinished">Szkript API</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1547"/>
        <source>Actions.py is empty !</source>
        <translation type="unfinished">Az actions.py üres!</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1578"/>
        <source>Build Successful</source>
        <translation type="unfinished">Művelet befejezve</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1579"/>
        <source>Build files created successfully.</source>
        <translation type="unfinished">Csomagbeállító fájlok mentve.</translation>
    </message>
    <message>
        <location filename="../cpp/mainwindow.cpp" line="1599"/>
        <source>There is no PSPEC file : %1 </source>
        <translation type="unfinished">A %1-hez nem tartozik PSPEC fájl!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cpp/main.cpp" line="53"/>
        <source>Translator failed to load : </source>
        <translation type="unfinished">Hiba a fordítás betöltése közben:</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="63"/>
        <source>Empty source class !</source>
        <translation type="unfinished">Nincs forrásosztály!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="72"/>
        <source>Empty package class !</source>
        <translation type="unfinished">Nincs csomagosztály!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="74"/>
        <source>Invalid key</source>
        <translation type="unfinished">Érvénytelen ellenőrzőösszeg!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="84"/>
        <source>Empty history update !</source>
        <translation type="unfinished">Nincs megadva átdolgozás!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="107"/>
        <source>From Source parser : %1</source>
        <translation type="unfinished">Hiba a %1 olvasásakor</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="112"/>
        <source>Can not find Source tag !</source>
        <translation type="unfinished">A &lt;Source&gt; tag nem található!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="129"/>
        <source>From Package parser : %1</source>
        <translation type="unfinished">Hiba a %1 olvasásakor</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="134"/>
        <source>Can not find Package tag !</source>
        <translation type="unfinished">A &lt;Package&gt; tag nem található</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="158"/>
        <source>From Update parser : %1</source>
        <translation type="unfinished">Hiba a %1 olvasásakor</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="170"/>
        <source>Can not find History tag !</source>
        <translation type="unfinished">A &lt;History&gt; tag nem található!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="177"/>
        <source>Can not find PISI tag !</source>
        <translation type="unfinished">A &lt;PISI&gt; tag nem található!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="184"/>
        <source>Empty pisi class while saving pisi class values to dom !</source>
        <translation type="unfinished">Hiba az értékek lekérése közben: a Pisi osztály üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="195"/>
        <source>Loaded xml file does not start with PISI !</source>
        <translation type="unfinished">Az XML fájl nem a &lt;PISI&gt; taggel kezdődik!</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="207"/>
        <source>From Source saver : %1</source>
        <translation type="unfinished">Hiba a %1 mentésekor:</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="225"/>
        <source>From Package saver : %1</source>
        <translation type="unfinished">Hiba a %1 mentésekor:</translation>
    </message>
    <message>
        <location filename="../cpp/pisi.cpp" line="244"/>
        <source>From Update saver : %1</source>
        <translation type="unfinished">Hiba a %1 mentésekor:</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="24"/>
        <source>Dom Element is null while loading to PisiPackage !</source>
        <translation type="unfinished">Hiba a PisiPackage betöltése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="50"/>
        <source>Dom Element is null while saving from PisiPackage to dom !</source>
        <translation type="unfinished">Hiba a PisiPackage lekérése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="72"/>
        <location filename="../cpp/pisispbase.cpp" line="230"/>
        <location filename="../cpp/pisiupdate.cpp" line="85"/>
        <source>%1 tag is mandatory but empty !</source>
        <translation type="unfinished">a %1 tag beállítása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="201"/>
        <source>Undefined tag name &quot;%1&quot; in PisiPackage::is_mandatory() !</source>
        <translation type="unfinished">Ismeretlen tag a kötelező PisiPackage tagek között: \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="204"/>
        <source>Undefined root_tag name &quot;%1&quot; in PisiPackage::is_mandatory() !</source>
        <translation type="unfinished">Ismeretlen root tag a kötelező PisiPackage tagek között: \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="218"/>
        <source>No Path in Files</source>
        <translation type="unfinished">Nincsen elérési útvonal beállítva!</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="225"/>
        <source>There is no fileType attribute in Path tag !</source>
        <translation type="unfinished">Nincsen fileType attribútum beállítva!</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="239"/>
        <location filename="../cpp/pisispbase.cpp" line="216"/>
        <location filename="../cpp/pisiupdate.cpp" line="64"/>
        <source>No %1 tag !</source>
        <translation type="unfinished">Nincsen %1 címke</translation>
    </message>
    <message>
        <location filename="../cpp/pisipackage.cpp" line="321"/>
        <source>Wrong files atribute name : %1</source>
        <translation type="unfinished">Hibás files név attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="25"/>
        <source>Dom Element is null while loading to PisiSource !</source>
        <translation type="unfinished">Hiba a PisiSource betöltése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="28"/>
        <source>No Homepage tag !</source>
        <translation type="unfinished">Nincsen &lt;Homepage&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="32"/>
        <source>No Packager tag !</source>
        <translation type="unfinished">Nincsen &lt;Packager&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="35"/>
        <source>No Packager::Name tag !</source>
        <translation type="unfinished">Nincsen &lt;Packager::Name&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="39"/>
        <source>No Packager::Email tag !</source>
        <translation type="unfinished">Nincsen &lt;Packager::Email&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="43"/>
        <source>No Archive tag !</source>
        <translation type="unfinished">Nincsen &lt;Archive&gt; tag</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="54"/>
        <location filename="../cpp/pisisource.cpp" line="76"/>
        <location filename="../cpp/pisispbase.cpp" line="310"/>
        <location filename="../cpp/pisispbase.cpp" line="434"/>
        <source>Can not convert to attribute !</source>
        <translation type="unfinished">Attribútum nem állítható be!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="91"/>
        <source>Dom Element is null while saving from PisiSource to dom !</source>
        <translation type="unfinished">Hiba a PisiSource lekérése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="102"/>
        <source>More than one packager info !</source>
        <translation type="unfinished">Egynél több csomagoló nem lehet!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="105"/>
        <source>There is no packager info !</source>
        <translation type="unfinished">Nincsen személyes információ megadva!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="119"/>
        <source>There is no archive !</source>
        <translation type="unfinished">Nincsen archívum megadva</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="180"/>
        <source>Homepage can not be empty !</source>
        <translation type="unfinished">Nincsen weboldal megadva!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="188"/>
        <source>Empty packager name !</source>
        <translation type="unfinished">Név megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="190"/>
        <source>Empty packager email !</source>
        <translation type="unfinished">E-mail cím megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="200"/>
        <source>Empty archive !</source>
        <translation type="unfinished">Az archívum üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="219"/>
        <source>Wrong archive atribute name : %1</source>
        <translation type="unfinished">Hibás archive név attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="231"/>
        <source>Wrong archive atribute index : %1</source>
        <translation type="unfinished">Hibás archive index attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="244"/>
        <source>Wrong patch atribute name : %1</source>
        <translation type="unfinished">Hibás patch név attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="258"/>
        <source>Wrong patch atribute index : %1</source>
        <translation type="unfinished">Hibás patch index attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="300"/>
        <source>Undefined tag name &quot;%1&quot; in PisiSource::is_mandatory() !</source>
        <translation type="unfinished">Ismeretlen root tag a kötelező PisiSource tagek között: \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="308"/>
        <source>Undefined tag name &quot;%1&quot;for packager tag in PisiSource::is_mandatory() !</source>
        <translation type="unfinished">Ismeretlen pacakger tag a kötelező PisiSource tagek között: \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../cpp/pisisource.cpp" line="311"/>
        <source>Undefined root_tag name &quot;%1&quot; in PisiSource::is_mandatory() !</source>
        <translation type="unfinished">Ismeretlen root tag a kötelező PisiSource tagek között: \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="28"/>
        <source>Dom Element is null while loading to PisiSPBase !</source>
        <translation type="unfinished">Hiba a PisiSPBase betöltése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="64"/>
        <source>Dom Element is null while saving from PisiSPBase to dom !</source>
        <translation type="unfinished">Hiba a PisiSPBase lekérése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="153"/>
        <source>Empty name !</source>
        <translation type="unfinished">Nincsen csomagnév!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="161"/>
        <source>Empty summary !</source>
        <translation type="unfinished">Nincsen összefoglaló!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="181"/>
        <source>Empty license !</source>
        <translation type="unfinished">Licenc megadása kötelező!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="253"/>
        <location filename="../cpp/pisispbase.cpp" line="264"/>
        <location filename="../cpp/pisispbase.cpp" line="275"/>
        <source>Error while creating dom element %1 in %2</source>
        <translation type="unfinished">Hiba történt a %1 DOM elem létrehozása közben %2-ben</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="261"/>
        <source>Trying to insert %1 after %2 but no %2 tag !</source>
        <translation type="unfinished">Nem lehet %1-t a %2 mögé beszúrni: a %2 tag üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="272"/>
        <source>Trying to insert %1 before %2 but no %2 tag !</source>
        <translation type="unfinished">Nem lehet %1-t a %2 elé beszúrni: a %2 tag üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="283"/>
        <source>Error creating text element with %1 in to the %2</source>
        <translation type="unfinished">A szövegelemet nem lehet létrehozni %1 és %2 között</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="361"/>
        <source>Wrong dependency atribute name : %1</source>
        <translation type="unfinished">Hibás függőségi attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="377"/>
        <source>No VersionReleaseToFrom attribute : %1</source>
        <translation type="unfinished">Nincs VersionReleaseToFrom attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisispbase.cpp" line="480"/>
        <source>Wrong aditional_files atribute name : %1</source>
        <translation type="unfinished">Rossz additional_files  név attribútum: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="23"/>
        <source>Dom Element is null while loading to PisiUpdate !</source>
        <translation type="unfinished">Hiba a PisiUpdate feltöltése közben: a DOM elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="49"/>
        <source>Dom Element is null while saving from PisiUpdate to dom !</source>
        <translation type="unfinished">Hiba a PisiUpdate lekérése közben: a DOm elem üres!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="82"/>
        <source>Error while creating dom element %1</source>
        <translation type="unfinished">%1 DOM elem nem hozható létre</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="89"/>
        <source>Error while creating dom text element with value = %1</source>
        <translation type="unfinished">Nem hozható létre DOM elem %1 értékkel</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="129"/>
        <source>Release number error : %1</source>
        <translation type="unfinished">Hibás kiadásszám: %1</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="137"/>
        <source>Empty update date !</source>
        <translation type="unfinished">Nincs dátum megadva az átdolgozáshoz!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="145"/>
        <source>Empty update version !</source>
        <translation type="unfinished">Nincs verzió megadva az átdolgozáshoz!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="153"/>
        <source>Empty update comment !</source>
        <translation type="unfinished">Nincs megjegyzés megadva az átdolgozáshoz!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="161"/>
        <source>Empty update packager name !</source>
        <translation type="unfinished">Nincs név megadva az átdolgozáshoz!</translation>
    </message>
    <message>
        <location filename="../cpp/pisiupdate.cpp" line="169"/>
        <source>Empty update packager email !</source>
        <translation type="unfinished">Nincs e-mail cím megadva az átdolgozáshoz!</translation>
    </message>
</context>
<context>
    <name>TranslationWidget</name>
    <message>
        <location filename="../ui/translationwidget.ui" line="14"/>
        <source>TranslationWidget</source>
        <translation type="unfinished">Fordítások</translation>
    </message>
    <message>
        <location filename="../ui/translationwidget.ui" line="35"/>
        <source>Language</source>
        <translation type="unfinished">Fordítás</translation>
    </message>
</context>
<context>
    <name>WorkspaceDialog</name>
    <message>
        <location filename="../ui/workspacedialog.ui" line="14"/>
        <source>Select PiSiDo Workspace</source>
        <translation type="unfinished">PiSiDo környezet beállítása</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="20"/>
        <source>Please select a workspace. All packages and their build files will be created in here.</source>
        <translation type="unfinished">Állítsa be a környezetet.</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="32"/>
        <source>Workspace</source>
        <translation type="unfinished">Munkakönyvtár</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="39"/>
        <source>Produced build files ad pisi packages will save under here</source>
        <translation type="unfinished">Ide kerülnek a beállító fájlok és a csomagok</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="49"/>
        <source>&amp;Browse</source>
        <translation type="unfinished">&amp;Tallózás</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="71"/>
        <source>You can change workspace form menu</source>
        <translation type="unfinished">Később a beállítások megváltoztathatóak</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="74"/>
        <source>Don&apos;t ask workspace again.</source>
        <translation type="unfinished">Ne jelenjen meg többet.</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="96"/>
        <source>&amp;Ok</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="106"/>
        <source>Exit application</source>
        <translation type="unfinished">Kilépés a programból</translation>
    </message>
    <message>
        <location filename="../ui/workspacedialog.ui" line="109"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">&amp;Kilépés</translation>
    </message>
    <message>
        <location filename="../cpp/workspacedialog.cpp" line="26"/>
        <source>Select Workspace</source>
        <translation type="unfinished">Munkakönyvtár kiválasztása</translation>
    </message>
</context>
</TS>
